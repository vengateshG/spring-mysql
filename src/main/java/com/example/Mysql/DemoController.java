package com.example.Mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DemoController {
	
	@Autowired 
	DemoService demoService;
	
	@RequestMapping(method=RequestMethod.GET)
	public String findAllDetails(ModelMap modelMap) {
		modelMap.put("adminuser", demoService.getUserDetails());
		System.out.print(modelMap);
		return "view";
	}	

}
