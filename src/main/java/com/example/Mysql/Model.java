package com.example.Mysql;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity 

@Table(name="demo")
public class Model {
	@Id
	private int age;
	private int empid;
	private String name;
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
