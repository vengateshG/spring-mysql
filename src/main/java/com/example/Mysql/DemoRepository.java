package com.example.Mysql;

import org.springframework.stereotype.Repository;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface DemoRepository extends CrudRepository<Model, Integer> {
	
}
